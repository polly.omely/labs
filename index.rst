Калькулятор
=======================================================
.. automodule:: main
    :members:
.. toctree::
    :maxdepth: 2
    :caption: Contents:
.. automodule:: calculator
    :members:
.. toctree::
    :maxdepth: 2
    :caption: Contents:
.. automodule:: calculatorfactory
    :members:
.. toctree::
    :maxdepth: 2
    :caption: Contents:
.. automodule:: historyprovider
    :members:
.. toctree::
    :maxdepth: 2
    :caption: Contents: