import os
import sys
sys.path.insert(0, os.path.abspath('./labs'))

project = 'labs'
copyright = '2021, Oleksandr Karnaukhov'
author = 'Oleksandr Karnaukhov'
release = '0.0.1'
extensions = ['sphinx.ext.autodoc', 'sphinx.ext.coverage', 'sphinx.ext.napoleon']
exclude_patterns = ['.tox', 'venv']
