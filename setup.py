from setuptools import setup


setup(
    name='labs',
    version='0.0.1',
    license='MIT',
    author='Sasha',
    author_email='karnaukhov@live.com',
    description='labs',
    packages=["labs"],
    entry_points={
        "console_scripts": [
            "labs = labs.main:main"
        ]
    }
)
