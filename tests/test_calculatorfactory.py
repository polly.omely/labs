"""Тестування фабрики калькулятора."""


import labs.calculatorfactory as calculatorfactory
import unittest.mock


def test_interaction():
    """Тестування взаємодії з користувачем."""
    with unittest.mock.patch('builtins.input', return_value=1):
        res = calculatorfactory.interaction()
        assert res == 1


def test_select_operation():
    """Тестування отримання функції."""
    assert calculatorfactory.select_operation(1).__name__ == 'sum'
    assert calculatorfactory.select_operation(2).__name__ == 'sub'
