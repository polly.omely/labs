"""Головна програма."""
from labs import historyprovider
from labs import calculatorfactory
hp = historyprovider.HistoryProvider()


def main():
    """Точка входу."""
    while(True):
        try:
            lines = hp.read_history()
            print("Історія операцій:")
            print(lines)
            print("-" * 20)

            a = float(input("Input a: "))
            b = float(input("Input b: "))
            function = calculatorfactory.select_operation(calculatorfactory.interaction())
            result = function(a, b)
            print(f"Результат: {result}")

            data = []
            data.append(function.__doc__)
            data.append("\n")
            data.append(f"Input a: {a}\n")
            data.append(f"Input b: {b}\n")
            data.append(f"Result: {result}\n")
            hp.add_to_history(data)
        except Exception as ex:
            print(ex)
        itr = input("Для завершення натисніть q, для продовження Enter\n")
        if(itr == "q"):
            break

    print("До побачення")


if __name__ == '__main__':
    main()
