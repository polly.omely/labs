"""'Фабрика' калькулятора."""


from labs import calculator


def interaction():
    """Взаємодія з користувачем для вибору методу розрахунків."""
    print("1 - Додавання\n")
    print("2 - Віднімання\n")
    print("3 - Множення\n")
    print("4 - Ділення\n")
    selected_operation = int(input("Виберіть операцію:"))
    return selected_operation


def select_operation(selected_operation):
    """
    Функція для вибору методя з кальтулатора.

    :returns: метод, яким будуть поводитись розрахунки
    """
    match selected_operation:
        case 1:
            return calculator.sum
        case 2:
            return calculator.sub
        case 3:
            return calculator.mult
        case 4:
            return calculator.div
        case _:
            raise Exception("Invalid operation")
