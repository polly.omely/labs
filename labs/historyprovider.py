"""Провайдер даних."""

import io


class HistoryProvider:
    """Провайдер даних."""

    FILE_NAME = "history.txt"

    def __init__(self):
        """Конструктор провайдера."""
        file = open(self.FILE_NAME, "a")
        file.close()

    def read_history(self):
        """Читання даних."""
        with io.open(self.FILE_NAME, "r", encoding="utf-8") as file:
            return file.read()

    def add_to_history(self, messages):
        """Запис в історію."""
        with io.open(self.FILE_NAME, "a", encoding="utf-8") as file:
            file.writelines(messages)
